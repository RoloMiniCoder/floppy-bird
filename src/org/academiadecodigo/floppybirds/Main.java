package org.academiadecodigo.floppybirds;

import org.academiadecodigo.floppybirds.game_entities.obstacles.ObstaclePool;
import org.academiadecodigo.floppybirds.keyboard.KeyboardManager;
import org.academiadecodigo.floppybirds.phases.Game;
import org.academiadecodigo.floppybirds.phases.GameOver;
import org.academiadecodigo.floppybirds.phases.Menu;

public class Main {
    public static void main(String[] args) {
        KeyboardManager kbManager = new KeyboardManager();
        kbManager.init();
        Menu intro = new Menu();
        GameOver outro = new GameOver();
        ObstaclePool pool = new ObstaclePool(10);
        Game game = new Game(pool);

        while (true){

            kbManager.setHandler(intro);
            intro.run();

            kbManager.setHandler(game);
            game.run();

            kbManager.setHandler(outro);
            outro.run();
        }
    }
}
