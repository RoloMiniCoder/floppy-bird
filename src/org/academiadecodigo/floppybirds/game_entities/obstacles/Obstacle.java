package org.academiadecodigo.floppybirds.game_entities.obstacles;

import org.academiadecodigo.floppybirds.game_entities.Player;
import org.academiadecodigo.floppybirds.phases.Game;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Obstacle {
    private final static int SPEED = 5;

    private final Picture topRepresentation;
    private final Picture botRepresentation;

    public Obstacle(ObstacleInfo info) {
        topRepresentation = new Picture(0,Game.PADDING,info.top);
        botRepresentation = new Picture(0,0,info.bot);
        botRepresentation.translate(0, Game.BOTTOM_LIMIT - botRepresentation.getMaxY());
        toInitialPosition();
    }

    public void show(){
        topRepresentation.draw();
        botRepresentation.draw();
    }

    public void hide(){
        topRepresentation.delete();
        botRepresentation.delete();
    }

    public boolean isTouchingLeft() {
        return botRepresentation.getX() <= Game.PADDING;
    }

    public void toInitialPosition(){
        int xDelta = 0;
        int yDelta = Game.RIGHT_LIMIT-topRepresentation.getMaxX();
        topRepresentation.translate(yDelta, xDelta);
        botRepresentation.translate(yDelta, xDelta);
    }

    public void move() {
        topRepresentation.translate(-SPEED, 0);
        botRepresentation.translate(-SPEED, 0);
    }

    public boolean isTouchingPlayer(Player player) {
        return playerTouchingTopSide(player) || playerTouchingBotSide(player);
    }

    private boolean playerTouchingBotSide(Player player) {
        return botRepresentation.getY() <= player.getMaxY()
                && botRepresentation.getX() <= player.getMaxX()
                && botRepresentation.getMaxX() >= player.getX()
                && botRepresentation.getMaxY() >= player.getY();
    }

    private boolean playerTouchingTopSide(Player player) {
        return topRepresentation.getY() <= player.getMaxY()
                && topRepresentation.getX() <= player.getMaxX()
                && topRepresentation.getMaxX() >= player.getX()
                && topRepresentation.getMaxY() >= player.getY();
    }
}
