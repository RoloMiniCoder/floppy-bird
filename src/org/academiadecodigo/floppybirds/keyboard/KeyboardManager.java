package org.academiadecodigo.floppybirds.keyboard;

import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

public class KeyboardManager implements KeyboardHandler {

    final private Keyboard kb;
    private Handler handler;

    public KeyboardManager() {
        kb = new Keyboard(this);
        init();
    }

    public void init(){
        for (KeySet k: KeySet.values()){
            KeyboardEvent e = new KeyboardEvent();
            e.setKey(k.KEY_NUM);
            e.setKeyboardEventType(k.TYPE);
            kb.addEventListener(e);
        }
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        handler.keyPressed(keyboardEvent);
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
        handler.keyReleased(keyboardEvent);
    }
}
