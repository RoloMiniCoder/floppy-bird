package org.academiadecodigo.floppybirds.phases;

import org.academiadecodigo.floppybirds.game_entities.Player;
import org.academiadecodigo.floppybirds.game_entities.obstacles.Obstacle;
import org.academiadecodigo.floppybirds.game_entities.obstacles.ObstaclePool;
import org.academiadecodigo.floppybirds.keyboard.Handler;
import org.academiadecodigo.floppybirds.utils.ScoreCounter;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.pictures.Picture;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Game implements Handler {

    final static public int PADDING = 10;
    //those are hard coded due to image size, more on this in explanation
    final static public int RIGHT_LIMIT = 1095 + PADDING;
    final static public int BOTTOM_LIMIT = 675 + PADDING;
    final static private int POINTS_PER_COLUMN = 5;

    final private int ADD_INTERVAL = 50;
    final private int PLAYER_X = 170;
    final private ObstaclePool obstaclePool;
    final private Picture background;
    final private Picture flames;
    final private Player player;
    final private List<Obstacle> obstacles;
    final private ScoreCounter counter;

    private boolean gameOver;

    public Game(ObstaclePool pool) {
        counter = new ScoreCounter(PADDING + 10, PADDING + 10);
        obstaclePool = pool;
        obstacles = new LinkedList<>();
        gameOver = false;
        background = new Picture(PADDING, PADDING, "resources/blue_sky_background.png");
        flames = new Picture(PADDING, 0, "resources/fire_bottom.png");
        flames.translate(0, BOTTOM_LIMIT - flames.getMaxY());
        player = new Player(PLAYER_X, BOTTOM_LIMIT / 2);
    }

    public void run() {
        try {
            background.draw();
            obstacles.add(obstaclePool.getOne());
            flames.draw();
            player.show();
            counter.show();

            int counter = 0;
            while (!gameOver) {
                Thread.sleep(15);
                counter++;

                player.move();
                if (player.getMaxY() >= BOTTOM_LIMIT) {
                    gameOver = true;
                    break;
                }
                moveObstacles();

                if (counter >= ADD_INTERVAL) {
                    obstacles.add(obstaclePool.getOne());
                    flames.delete();
                    flames.draw();
                    counter = 0;
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            cleanup();
        }
    }

    public void cleanup() {
        background.delete();
        flames.delete();
        player.hide();
        player.reset();
        counter.reset();
        counter.hide();
        Iterator<Obstacle> it = obstacles.iterator();
        while (it.hasNext()) {
            Obstacle o = it.next();
            it.remove();
            obstaclePool.giveBack(o);
        }
        gameOver = false;
    }

    private void moveObstacles() {
        Iterator<Obstacle> it = obstacles.iterator();
        while (it.hasNext()) {
            Obstacle o = it.next();
            o.move();
            if (o.isTouchingPlayer(player)) {
                gameOver = true;
            }
            if (o.isTouchingLeft()) {
                counter.increment(POINTS_PER_COLUMN);
                it.remove();
                obstaclePool.giveBack(o);
            }
        }
    }

    @Override
    public void keyPressed(KeyboardEvent event) {
        switch (event.getKey()) {
            case KeyboardEvent.KEY_SPACE:
                player.jump();
                break;
            case KeyboardEvent.KEY_Q:
                System.exit(0);
        }
    }

    @Override
    public void keyReleased(KeyboardEvent event) {
    }
}
