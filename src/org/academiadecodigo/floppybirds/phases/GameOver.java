package org.academiadecodigo.floppybirds.phases;

import org.academiadecodigo.floppybirds.keyboard.Handler;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class GameOver implements Handler {

    final private Picture text;
    final private Picture background;
    private boolean idling;

    public GameOver() {
        background = new Picture(Game.PADDING, Game.PADDING, "resources/blue_sky_background.png");
        text = new Picture(Game.PADDING, Game.PADDING, "resources/game_over_v2.png");
        idling = true;
    }

    public void run(){
        background.draw();
        text.draw();

        while(idling){
            System.out.println("we'll learn how to deal with this soon");
        }
        cleanup();
    }

    private void cleanup(){
        text.delete();
        background.delete();
        idling = true;
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        switch (keyboardEvent.getKey()){
            case KeyboardEvent.KEY_R:
                idling = false;
                break;
            case KeyboardEvent.KEY_Q:
                System.exit(0);
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
    }
}
