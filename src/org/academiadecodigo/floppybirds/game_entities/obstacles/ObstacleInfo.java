package org.academiadecodigo.floppybirds.game_entities.obstacles;

public enum ObstacleInfo {
    ONE("resources/towers/tower1_top.png", "resources/towers/tower1_bot.png"),
    TWO("resources/towers/tower2_top.png", "resources/towers/tower2_bot.png"),
    THREE("resources/towers/tower3_top.png", "resources/towers/tower3_bot.png");

    public final String top;
    public final String bot;

    ObstacleInfo(String top, String bot) {
        this.top = top;
        this.bot = bot;
    }

    public static ObstacleInfo getRandom() {
        return values()[(int)(Math.random() * values().length)];
    }
}
