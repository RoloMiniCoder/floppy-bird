package org.academiadecodigo.floppybirds.phases;

import org.academiadecodigo.floppybirds.keyboard.Handler;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Menu implements Handler {

    final private Picture background;
    private boolean idling;

    public Menu() {
        background = new Picture(Game.PADDING, Game.PADDING, "resources/main_menu.png");
        idling = true;
    }

    public void run() {
        background.draw();

        while (idling) {
            System.out.println("we'll learn how to deal with this soon");
        }
        cleanup();
    }

    private void cleanup() {
        background.delete();
        idling = true;
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_ENTER:
                idling = false;
                break;
            case KeyboardEvent.KEY_Q:
                System.exit(0);
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
    }
}
