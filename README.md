# Floppy-bird !
##### ... a flapping floppy.

This is a complete from scrath refactoring from the game [Floppy Bird][floppy-bird-gitlab], from [Guilherme][guilherme-gitlab], [Ricardo][ricardo-gitlab] and [Daniel][daniel-gitlab].

Made as an instructional example to showcase some different strategies, thoughts and approaches at the same problem/behaviour.

[floppy-bird-gitlab]:<https://github.com/Noble3some/game.git>
[guilherme-gitlab]:<https://gitlab.com/ShowMeTheGita>
[ricardo-gitlab]:<https://gitlab.com/RicardoRKS>
[daniel-gitlab]:<https://gitlab.com/DanielPinto15>