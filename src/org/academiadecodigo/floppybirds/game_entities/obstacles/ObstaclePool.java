package org.academiadecodigo.floppybirds.game_entities.obstacles;


import java.util.LinkedList;

public class ObstaclePool {
    private LinkedList<Obstacle> pool;

    public ObstaclePool(int size){
        pool = new LinkedList<>();

        for (int i = 0; i < size; i++) {
            pool.add(new Obstacle(ObstacleInfo.getRandom()));
        }
    }

    public Obstacle getOne() {
        Obstacle o = pool.poll();
        o.show();
        return o;
    }

    public void giveBack(Obstacle o){
        o.hide();
        o.toInitialPosition();
        pool.push(o);
    }
}
